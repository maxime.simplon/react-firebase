import React, { useState, useEffect } from "react";
import firebase from "../components/firebase";
import marker from "../marker.png";
import jobSearch from "../job-search.png";
import star from "../star.png";
import NavbarPublic from "../components/navbarPublic";
import Search from "../components/search";
import email from "../email.png";
import "../components/accordion.css";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemButton
} from "react-accessible-accordion";

import moment from "moment";
import "moment/locale/fr";
moment.locale("fr");

function useOffers() {
  const [offers, setOffers] = useState([]);

  useEffect(() => {
    firebase
      .firestore()
      .collection("offers")
      .where("category", "==", "Développement web")
      .onSnapshot(snapshot => {
        const newOffers = snapshot.docs.map(doc => ({
          id: doc.id,
          ...doc.data()
        }));
        setOffers(newOffers);
      });
  }, []);
  return offers;
}

const FirstFilterList = () => {
  const offers = useOffers();
  return (
    <div>
      <NavbarPublic />
      <div className="container">
        <br />
        <center>
          <h1 style={{paddingTop:"40px"}}>
            Liste des offres <img src={jobSearch} alt="job-search" />
          </h1>
          <hr />

          <Search />
          <br />
          {offers.map((offer, i) => (
            <div key={i} className=" jumbotron shadow p-3 mb-5 rounded">
              <span>
                <p>
                  <img src={star} alt="star" /> <b>Intitulé:</b> {offer.title}{" "}
                  <img src={star} alt="star" />
                </p>
                <p>
                  <img src={marker} alt="marker" />
                  <b>Nom de l'entreprise:</b> {offer.entreprise}
                </p>
                <hr />
                <Accordion allowZeroExpanded={true}>
                  <AccordionItem>
                    <AccordionItemHeading>
                      <AccordionItemButton
                        style={{ cursor: "pointer" }}
                        className="btn btn-warning shadow  rounded"
                      >
                        {" "}
                        Voir la description
                      </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel
                      style={{ whiteSpace: "pre-wrap", textAlign: "left" }}
                    >
                      <p>{offer.content}</p>
                    </AccordionItemPanel>
                  </AccordionItem>
                  <AccordionItem />
                </Accordion>
                <hr />{" "}
                <p>
                  <img src={email} alt="email" /> <b>Pour postuler: </b>
                  {offer.contact}
                </p>
                <p style={{ color: "grey" }}>
                  Posté {moment(offer.dateTime.toDate()).fromNow()}
                </p>
              </span>
            </div>
          ))}
        </center>
      </div>
    </div>
  );
};

export default FirstFilterList;
