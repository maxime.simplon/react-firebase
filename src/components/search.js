import React from "react";



const Search = () => {
  // const offers = useOffers();

  // function onSubmit(e) {
  //   e.preventDefault();

  return (
    <div className="container">
      <form className="form-inline mr-auto ">
        <input
          className="form-control mr-sm-2  shadow p-2 rounded"
          type="text"
          placeholder="Rechercher"
          aria-label="Search"
        />
        <button
          className="btn btn-outline-warning btn-rounded btn-sm my-0  shadow p-2 rounded"
          type="submit"
        >
          Go !
        </button>
      </form>
    </div>
  );
};

export default Search;
