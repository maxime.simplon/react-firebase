import React, { useState } from "react";
import firebase from "../components/firebase";

const OfferForm = () => {
  const uid = firebase.auth().currentUser.uid;
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [contact, setContact] = useState("");
  const [entreprise, setEntreprise] = useState("");
  const [category, setCategory] = useState("")
  const [ownerUid] = useState(uid);
  const [dateTime] = useState(new Date());

  function onSubmit(e) {
    e.preventDefault();

    firebase
      .firestore()
      .collection("offers")
      .add({
        title,
        content,
        contact,
        entreprise,
        category,
        dateTime,
        ownerUid,
      })
      .then(
        () => setTitle(""),
        setContent(""),
        setContact(""),
        setEntreprise(""),
        setCategory("")
      );
  }

  return (
    <div className="container">

        <h4>Ajouter une offre</h4>
      <div className="jumbotron">
        <form onSubmit={onSubmit}>
          <div className="form-group">
            <label>Nom de l'entreprise</label>
            <input
              type="text"
              className="form-control"
              id="title"
              placeholder="Entrer nom"
              value={entreprise}
              onChange={e => setEntreprise(e.currentTarget.value)}
              required
            />
          </div>
          <div className="form-group">
            <label>Intitulé de l'offre</label>
            <input
              type="text"
              className="form-control"
              id="title"
              placeholder="Entrer titre"
              value={title}
              onChange={e => setTitle(e.currentTarget.value)}
              required
            />
          </div>

          <div className="form-group">
      <label htmlFor="inputState">Catégorie</label>
      <select id="inputState" className="form-control" value={category} onChange={e => setCategory(e.currentTarget.value)}
 >
        <option className= "value">Choisir...</option>
        <option>Développement web</option>
        <option>Graphisme</option>
        <option>Rédaction de contenu</option>
        <option>Autre</option>
      </select>
    </div>

          <div className="form-group">
            <label>Description</label>
            <textarea 
              className="form-control"
              id="exampleFormControlTextarea1"
              placeholder="Description..."
              rows="5"
              value={content}
              onChange={e => setContent(e.currentTarget.value)}
              required
            />
          </div>
          <div className="form-group">
            <label>Pour vous contacter</label>
            <input 
              type="email"
              className="form-control"
              id="email"
              placeholder="Entrer email"
              value={contact}
              onChange={e => setContact(e.currentTarget.value)}
              required
            />
          </div>
          <button type="submit" className="btn btn-success">
            Publier
          </button>
        </form>
      </div>
    </div>
  );
};

export default OfferForm;
