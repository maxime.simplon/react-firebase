import React from "react";
import firebase from "../components/firebase";
import FileUploader from "react-firebase-file-uploader";

const UploadFile = () => {
  const uid = firebase.auth().currentUser.uid;

  function handleUploadSuccess(filename) {
    firebase
      .storage()
      .ref("images")
      .child(filename)
      .getDownloadURL()
      .then(url =>
        firebase
          .firestore()
          .collection("logos")
          .add({
            url,
            uid
          })
      );
  }

  return (
    <div>
      <FileUploader
        accept="image/*"
        name="logo"
        randomizeFilename
        storageRef={firebase.storage().ref("images")}
        onUploadSuccess={handleUploadSuccess}
      />
    </div>
  );
};

export default UploadFile;
