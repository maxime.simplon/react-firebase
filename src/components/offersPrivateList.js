import React, { useState, useEffect } from "react";
import firebase from "../components/firebase";
import "../components/accordion.css";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemButton,
} from "react-accessible-accordion";
import moment from "moment";
// import UploadFile from "../components/upload";
import "moment/locale/fr";
moment.locale("fr");

//DISPLAY LOGO

// function useFiles() {
//   const [files, setFiles] = useState([]);
//   useEffect(() => {
//     const userId = firebase.auth().currentUser.uid;
//     firebase
//       .firestore()
//       .collection("logos")
//       .where("uid", "==", userId)
//       .onSnapshot(snapshot => {
//         const newFiles = snapshot.docs.map(doc => ({
//           id: doc.id,
//           ...doc.data(),
//         }));
//         setFiles(newFiles);
//       });
//   }, []);
//   return files;
// }

function useOffers() {
  const [offers, setOffers] = useState([]);
  useEffect(() => {
    const userId = firebase.auth().currentUser.uid;
    firebase
      .firestore()
      .collection("offers")
      .where("ownerUid", "==", userId)
      .onSnapshot(snapshot => {
        const newOffers = snapshot.docs.map(doc => ({
          id: doc.id,
          ...doc.data(),
        }));
        setOffers(newOffers);
      });
  }, []);
  return offers;
}

const OffersPrivateList = () => {
  const db = firebase.firestore();
  const offers = useOffers();
  // const files = useFiles();

  return (
    <div className="container">
      <h4 id="offers">Vos offres publiées</h4>

      {/* {files.map((file, j) => (
            <div key={j}>
        <img style={{height:"100px", width:"100px"}} src={file.url}></img>
        </div>
      ))} */}
      {offers.map((offer, i) => (
        <div key={i}>
          <div className="card shadow p-3 mb-5  rounded">
            <p>
              <b>Intitulé:</b> {offer.title}
            </p>
            <p>
              <b>Nom de l'entreprise:</b> {offer.entreprise}
            </p>
            <p>
              <b>Catégorie:</b> {offer.category}
            </p>
            <Accordion allowZeroExpanded={true}>
              <AccordionItem>
                <AccordionItemHeading>
                  <AccordionItemButton
                    style={{ cursor: "pointer" }}
                    className="btn btn-sm btn-secondary shadow rounded"
                  >
                    Afficher la description
                  </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel
                  className="card"
                  style={{ whiteSpace: "pre-wrap", textAlign: "left" }}
                > <br />
                  <p>{offer.content}</p>
                </AccordionItemPanel>
              </AccordionItem>
              <AccordionItem />
            </Accordion>
            <br />
            <p>
              <b>Pour postuler:</b> {offer.contact}
            </p>
            <p style={{ color: "grey" }}>
              Posté {moment(offer.dateTime.toDate()).fromNow()}
            </p>
            <button
              style={{ width: "200px" }}
              onClick={() => {
                if (window.confirm("Confirmer la suppression ?")) {
                  db.collection("offers")
                    .doc(offer.id)
                    .delete();
                }
              }}
              className="btn btn-danger shadow rounded"
            >
              Supprimer
            </button>
          </div>
        </div>
      ))}
    </div>
  );
};

export default OffersPrivateList;
