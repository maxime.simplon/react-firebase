import React from "react"
import firebase from "firebase/app"
import "firebase/auth"

const NavBarPrivate = () => {
    return (
<div >
<nav style={{position:"fixed", zIndex:"1", width:"100%"}} className="navbar navbar-expand-lg navbar-light bg-dark">
  <a className="navbar-brand" href="/index" style={{color:"rgb(255, 193, 7)"}}>Nomajob</a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarNav">
    <ul className="navbar-nav">
      <li className="nav-item active">
        <a className="nav-link" href="/index" style={{color:"white"}}>Home <span className="sr-only">(current)</span></a>
      </li>
      <li className="nav-item ">
        <a className="nav-link" href="#add" style={{color:"white"}}>Ajouter une offre</a>
      </li>
      <li className="nav-item ">
        <a className="nav-link" href="#offers" style={{color:"white"}}>Vos offres</a>
      </li>
      <button className="btn btn-sm btn-light" onClick = {() => firebase.auth().signOut()}>Déconnexion</button>

    </ul>
  </div>
</nav>
</div>
    )
}

export default NavBarPrivate