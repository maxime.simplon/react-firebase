import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
import "firebase/storage";

const config = {
  apiKey: "AIzaSyCIs2a80oZmw1JtXTdiwd76dGYyiY_1UF4",
  authDomain: "gatsby-nomajob.firebaseapp.com",
  databaseURL: "https://gatsby-nomajob.firebaseio.com",
  projectId: "gatsby-nomajob",
  storageBucket: "gatsby-nomajob.appspot.com",
  messagingSenderId: "874028568757",
  appId: "1:874028568757:web:2467ea477f706acd"
};

firebase.initializeApp(config);
const storage = firebase.storage();

export {storage, firebase as default };
