import React, { useState, useEffect } from "react";
import firebase from "../components/firebase";
import marker from "../marker.png";
import star from "../star.png";
import email from "../email.png";
import category from "../category.png";
import "../components/offers.css";
import "../components/accordion.css";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemButton,
} from "react-accessible-accordion";
import moment from "moment";
import "moment/locale/fr";
moment.locale("fr");

function useOffers() {
  const [offers, setOffers] = useState([]);
  //  const [currentPage, setCurrentPage ] = useState(1)
  //  const [offersPerPage, setOffersPerPage] = useState(5)

  // const [isFetching, setIsFetching] = useState(false);

  // useEffect(() => {
  //   window.addEventListener("scroll", handleScroll);
  //   return () => window.removeEventListener("scroll", handleScroll);
  // }, []);

  // function handleScroll() {
  //   if (
  //     window.innerHeight + document.documentElement.scrollTop !==
  //     document.documentElement.offsetHeight
  //   )
  //     return;
  //   setIsFetching(true);
  // }
  // useEffect(() => {
  //   if (!isFetching) return;
  //   fetchMoreListItems();
  // }, [isFetching]);
  // function fetchMoreListItems() {
  //   setTimeout(() => {
  //     setOffers(prevState => [
  //       ...prevState,
  //     ]);
  //     setIsFetching(false);
  //   }, 2000);
  // }




  useEffect(() => {
   firebase
      .firestore()
      .collection("offers")
      .orderBy("dateTime", "desc")
      .limit(5)

    // first.get().then(function(documentSnapshots) {
    // Get the last visible document
    // const lastVisible =
    //   documentSnapshots.docs[documentSnapshots.docs.length - 1];
    .get().then(function(snapshot) {
      const newOffers = snapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data(),
      }));
      setOffers(newOffers);
    });
    // const next = firebase
    //   .firestore()
    //   .collection("offers")
    //   .orderBy("dateTime", "desc")
    //   .startAfter(lastVisible);

    // next.get().then(function(snapshot) {
    //   const newOffers = snapshot.docs.map(doc => ({
    //     id: doc.id,
    //     ...doc.data(),
    //   }));
    //   setOffers(newOffers);
    // });
  }, []);

  // const IndexOfLastPost = currentPage * offersPerPage
  // const IndexOfFirstPost = IndexOfLastPost - offersPerPage
  // const datas = offers.slice(IndexOfFirstPost, IndexOfLastPost)


  return offers;
}





const OffersPublicList = () => {
 
  const datas = useOffers();

  return (
    <div className="container">
      <center>
        <h5>Les dernières offres publiées</h5>
        <br />
        {datas.map((data, i) => (
          <div key={i} className=" jumbotron shadow p-3 mb-5 rounded">
            <span>
              <p>
                <img src={star} alt="star" /> <b>Intitulé:</b> {data.title}{" "}
                <img src={star} alt="star" />
              </p>
              <p>
                <img src={marker} alt="marker" />
                <b>Nom de l'entreprise:</b> {data.entreprise}
              </p>
              <p>
                <img src={category} alt="category" />
                <b> Catégorie:</b> {data.category}
              </p>
              <hr />

              <Accordion allowZeroExpanded={true}>
                <AccordionItem>
                  <AccordionItemHeading>
                    <AccordionItemButton
                      style={{ cursor: "pointer" }}
                      className="btn btn-warning shadow rounded"
                    >
                      {" "}
                      Voir la description
                    </AccordionItemButton>
                  </AccordionItemHeading>
                  <AccordionItemPanel
                    style={{ whiteSpace: "pre-wrap", textAlign: "left" }}
                  >
                    <p>{data.content}</p>
                  </AccordionItemPanel>
                </AccordionItem>
                <AccordionItem />
              </Accordion>
              <hr />
              <p>
                <img src={email} alt="email" /> <b>Pour postuler: </b>
                {data.contact}
              </p>
              <p style={{ color: "grey" }}>
                Posté {moment(data.dateTime.toDate()).fromNow()}
              </p>
            </span>
          </div>
        ))}
      </center>

     </div>
)}


export default OffersPublicList;
