import React from "react";

const filter = () => {
  return (
    <div className="dropdown">
      <button
        className="btn btn-warning dropdown-toggle shadow rounded "
        type="button"
        id="dropdownMenuButton"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        Choisir une catégorie
      </button>
      <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a className="dropdown-item" href="/search/dev">
          Développement web
        </a>
      </div>
    </div>
  );
};

export default filter;
