import React from "react";
import { Link } from "react-router-dom";
import Footer from "../components/footer"
import "../App.css"


const Index = () => {
  
  return (
    <div className="bg">
      <center>
        <h1 style={{paddingTop:"10rem", color:"white"}}><b>Bienvenue sur<span style={{color:"rgb(255, 193, 7)"}}> Nomajob !</span></b></h1>
        <h5 style={{color:"white"}}>Votre plateforme d'offres d'emploi en télétravail</h5>
<br />
        <Link to="/candidat">
          <button className="btn btn-warning shadow rounded ">Espace candidat</button>{" "}
        </Link>

        <Link to="/recruteur">
          <button className="btn btn-light shadow rounded">Espace recruteur</button>
        </Link>
      </center>
      <Footer />
    </div>
  );
};

export default Index;
