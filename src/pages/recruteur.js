import React, { Component } from "react";
import NavbarPrivate from "../components/navbarPrivate";
import NavbarPublic from "../components/navbarPublic";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import firebase from "../components/firebase";
import OfferForm from "../components/offerForm";
import OffersPrivateList from "../components/offersPrivateList";

class Recruteur extends Component {
  state = {
    isSignedIn: false,
  };

  uiConfig = {
    signInFlow: "popup",
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.EmailAuthProvider.PROVIDER_ID,
    ],
    callbacks: {
      signInSuccessWithAuthResult: () => false,
    },
  };

  componentDidMount = () => {
    const unsubscride = firebase.auth().onAuthStateChanged(user => {
      this.setState({ isSignedIn: !!user });
    });
    return unsubscride;
  };

  render() {
    return (
      <div >
        {this.state.isSignedIn ? (
          <div>
            {" "}
            <NavbarPrivate />
            <br />
            <center>
              <h1 style={{paddingTop:"40px"}} >
                Bienvenue dans votre espace{" "}
                {firebase.auth().currentUser.displayName}
              </h1>
            </center>
            <hr />
            <br />
            <div id="add">
            <OfferForm />
            </div>
            <br />
            <OffersPrivateList />

          </div>
        ) : (
          <div>
            <NavbarPublic />
            <br />
            <center>
              <h1 style={{paddingTop:"40px"}}>Merci de vous identifier</h1>
            </center>
            <hr />
            <StyledFirebaseAuth
              uiConfig={this.uiConfig}
              firebaseAuth={firebase.auth()}
            />
          </div>
        )}
      </div>
    );
  }
}

export default Recruteur;
