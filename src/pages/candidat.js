import React from "react";
import NavbarPublic from "../components/navbarPublic";
import OffersPublicList from "../components/offersPublicList";
import Search from "../components/search";
import jobSearch from "../job-search.png";
import Filter from "../components/filter";
import "../App.css";

const Candidat = () => {
  return (
    <div>
      <NavbarPublic />
      <div className="container">
        <br />
        <center>
          <h1 style={{paddingTop:"40px"}}>
            Liste des offres <img src={jobSearch} alt="job-search" />
          </h1>
          <hr />
        </center>

        <Search />
        <br />
        <Filter />
        <br />
        <OffersPublicList />
      </div>
    </div>
  );
};

export default Candidat;
