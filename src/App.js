import React from "react";
import { Redirect, Route, BrowserRouter, Switch } from "react-router-dom";
import Index from "./pages/index";
import Recruteur from "./pages/recruteur";
import Candidat from "./pages/candidat";
import ProfilePage from "./components/upload";
import UploadFile from "./components/upload";
import FirstFilterList from "./components/firstFilterList";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/index" />} />
          <Route path="/index" component={Index} />
          <Route path="/upload" component={UploadFile} />
          <Route path="/candidat" component={Candidat} />
          <Route path="/recruteur" component={Recruteur} />
          <Route path="/upload" component={ProfilePage} />
          <Route path="/search/dev" component={FirstFilterList} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default App;
